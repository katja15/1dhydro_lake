% This is as poisson_solution.m but with more complex BCs (and other manufactured solution
% and domain).
%
% This solves the Poisson equation in 1D on the domain [0, 1]
%
% d/dx( - k du/dx)) - f = 0
%
% k = 1, f = k*cos(x)
%
% with boundary conditions:
%  Dirichlet: u = 1 at x0 and du/dx = -k*sin(x1) at x1
%
% This has the exact solution cos(x).  
%
% This equation can, for instance, be used to model steady state heat flow with u the
% temperature, q=-k du/dx the heat flux and f the heat input.
%
% Weak form:
% int( grad theta . k grad u) - int( theta f) - [theta k u] = 0
%
% where the boundary term [theta k u] = 0 - (-k*sin(x1)) for this example.

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

%% physical parameters
k = 1;

%% Manufactured solution
% This is the solution we want to have:
manusol = @(x) cos(x);
% Forcing which produces the manufactured solution.  
% (Plug manusol into the equation, differentiate and solve for f)
force_fn = @(x) k*cos(x)

%% domain and mesh
n  = 10;  % Increasing the number of nodes decreases the error.
x0 = 0;
x1 = 1;

%% Boundary conditions
BCtype = [1,2]'; % 1==Dirichlet, 2==Neumann
BCval = [1,-k*sin(x1)]';  % value: u , du/dx

[p.connect, p.coords, p.n_nodes, p.anodes, p.neumann_nodes]...
    = make_mesh(x0, x1, n, BCtype);

%% Finite element setup
[p.int_nn, p.int_ne, p.int_ee, p.int_ne_bdy, p.mean_en, p.Dx_en] ...
    = sparseFEM(p.connect, p.coords, p.neumann_nodes);

%% Residual
% As in hydro-example we define the force/source on the elements (staggered grid):
f = force_fn(p.mean_en*p.coords);

res = @(u) ...
        p.Dx_en(:,p.anodes)' *p.int_ee* k *p.Dx_en(:,:) * [BCval(1); u] ...  % note the Dirichlet node
        - p.int_ne(p.anodes,:) * f...
        - p.int_ne_bdy(p.anodes,:) * BCval(BCtype==2); % Neumann BC

% initial guess for fsolve
u0 = p.coords(2:end) * 0 + 1;
% solve
u = fsolve(res, u0);
% Note: as this is a linear equation it could be solved more efficiently.  
% But this way is closer to the subglacial hydrology equations.

% add boundary points
u = [1;u];

%% plot and compare to exact solution
figure
subplot(2,1,1);
plot(p.coords, manusol(p.coords), 'r')
hold
plot(p.coords, u)
legend('exact solution', 'numeric solution')
subplot(2,1,2);
plot(p.coords, manusol(p.coords) - u)
title('Difference exact - numeric solution')
