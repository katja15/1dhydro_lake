% Short-cut for animation
%
% Set pau to something to slow it down, set inds for just a few of the steps

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('pau')
    pau = [];
end
if ~exist('inds')
    inds = [];
end
animate(para,{ phi/1e6, S, -Q }, pau, inds, {'\phi (MPa)', 'S (m^2)', 'Q (m^3/s)'})
