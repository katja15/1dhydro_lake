function p = model_para_dome(x0, x1, num)
% para = model_para_dome(x0, x1, num)
%
% Returns a structure containing all the adjustable model parameters

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

% uses model_para for defaults:
p = model_para(x0, x1, num);

%% Boundary conditions
p.BCtype = [1,1]'; % 1==Dirichlet, 2==Neumann
p.BCval = [1,1]';  % value

%% mesh
[p.connect, p.coords, p.n_nodes, p.anodes, p.neumann_nodes]...
    = make_mesh(x0, x1, num, p.BCtype);

%% physical
nil = p.coords * 0;  % zeros for all nodes

p.M = 10e-3/x1^2 * (p.coords-x1/2).^2;  % source term [m^2/s]
p.sigma = nil + p.width*1e-10; % storage capacity per unit length [m^2]
                               % probably around 1e-8 to 1e-10 * width of glacier


%% dome
p.H = -(p.coords-x0).*(p.coords-x1);
p.H = 1000/max(p.H) * p.H + 10;

%% Initial conditions
p.S_IC = 0.35 + nil(1:end-1);

%% Time
p.tspan = [0:0.1:20]*p.day;
