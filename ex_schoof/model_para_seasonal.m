function p = model_para_seasonal(x0, x1, num)
% para = model_para_seasonal(x0, x1, num)
%
% Returns a structure containing all the adjustable model parameters

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

% uses model_para for defaults:
p = model_para(x0, x1, num);

%% physical
nil = p.coords * 0;  % zeros for all nodes

p.sigma = nil + p.width*1e-8; % storage capacity per unit length [m^2]
                               % probably around 1e-8 to 1e-10 * width of glacier

p.amp = 0.4/p.day*p.width;
p.lapse = -0.041/p.day/100*p.width;
p.basal = 0.05/p.year*p.width;
p.M = @(t) double(seasonal_source(t, p)); % double() is a trick to make anonymous functions faster

%% Initial conditions
p.S_IC = 0.85 + nil(1:end-1);

%% Time
p.tspan = [0:1:365]*p.day;
