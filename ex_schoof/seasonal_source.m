function out = seasonal_source(t, p)
%  out = seasonal_source(t, para)
% 
% Makes a seasonally varying source.  para must contain:
%  amp -- amplitude at sea level, typical: 0.2/para.day*para.width
%  lapse -- lapse rate for melt, typical:  -0.01/para.day/100*para.width
%  basal -- basal melt rate, typical: 0.05/para.year*para.width

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

out = -p.amp * cos(2*pi*t/p.year);

% lapse it to all elevations
out = out + p.lapse * p.H;

out(out<0) = 0;

% add basal melt
out = out + p.basal;