% This runs the model.  
% Uncomment different model_para* calls for different scenarios.

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

%% model parameters
num = 50;
x0 = 0;
x1 = 10e3;

%% choose one of the parameter files:
%para = model_para(x0, x1, num);
%para = model_para_seasonal(x0, x1, num);
%para = model_para_dome(x0, x1, num);
para = model_para_jokulhlaup(100);

para = proc_para(para);

%% run it
if para.octave % using octave
    % only model_para_seasonal works so far with Octave.
    objfun = @(y, ydot, t) objectivefun(t, y, para) - para.mass*ydot;
    
    ydot0 = objectivefun(para.tspan(1), para.IC, para);
    [out, out_dot, istate, msg] = daspk(objfun, para.IC, ydot0, para.tspan);
    t = para.tspan;
    if istate<0
        disp('Solve not successful with message:')
        disp(msg)
        error('Apbort')
    end
else % using matlab 
    % The objective function, this is where all the physics is,
    % except some is also in the mass-matrix which is set in proc_para.
    objfun = @(t, y) double(objectivefun(t, y, para));
    
    %% solve
    [t, out] = ode15s(objfun, para.tspan, para.IC, para.odeopts);
end

% unwrap
S = out(:,1:para.n_nodes-1)';
phi = zeros(para.n_nodes,size(out,1));
phi(para.anodes,:) = out(:,para.n_nodes:end)'; % on nodes
phi(~para.anodes,:) = repmat(para.BCval(para.BCtype==1), 1, size(out,1)); % set Dirichlet BC
para.tspan = t;


%% post-processing
[Q, Xi, creep, melt, N, grad_phi, channelized, stored, tot_stored,...
 source, tot_source, x, x2] = postproc(phi, S, para);
