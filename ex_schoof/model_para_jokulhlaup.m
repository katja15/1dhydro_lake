function p = model_para_jokulhlaup(num)
% para = model_para_jokulhlaup(num)
%
% Returns a structure containing all the adjustable model parameters

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

x0 = 0;
x1 = 10e3;

% uses model_para for defaults:
p = model_para(x0, x1, num);

%% Boundary conditions
p.BCtype = [1,2]'; % 1==Dirichlet, 2==Neumann
p.BCval = [1,-7]';  % value

%% mesh
[p.connect, p.coords, p.n_nodes, p.anodes, p.neumann_nodes]...
    = make_mesh(x0, x1, num, p.BCtype);

%% physical
nil = p.coords * 0;  % zeros for all nodes

p.sigma = nil + p.width*1e-8; % storage capacity per unit length per unit pressure [m^2/Pa]
                              % probably around 1e-8 to 1e-10 * width of glacier

%% dome
maxheight = 300;
p.H = -(p.coords-x0).*(p.coords-(x1+2000));
p.H = maxheight/max(p.H) * p.H + 10;

% seasonal input
p.amp = 0.6/p.day*p.width;
%p.lapse = -0.041/100 /p.day*p.width;
p.lapse = -0.6/maxheight /p.day*p.width;
p.basal = 0.1/p.year*p.width;
p.M = @(t) double(seasonal_source(t, p)); % double() is a trick to make anonymous functions faster

% $$$ % steady input
% $$$ p.M = nil + 30/1e4; % source term [m^2/s]
% $$$ p.M(p.H>0.8*maxheight) = 0; %0.01/1e4;

%% lake
% make sigma large to create a "lake" of total volume 1000e6 m^3 on the last node (that
% volume corresponds to a lake level equal to ice thickness)
dx = p.coords(end)-p.coords(end-1);
p.sigma = p.sigma*0 + 1e-10;  % set to almost zero everywhere
p.sigma(end) = 1000e6/(p.H(end)*p.rho_w*p.g * dx); % except at the lake

% to be able to fill the lake, p.h needs to be super small?!
p.h = 0.01;

%% Initial conditions
%p.phi_IC = maxheight/3*p.rho_w*p.g + nil;  % if p.sigma==0 this is an initial guess for phi
%p.S_IC = 0.85 + nil(1:end-1);
p.S_IC = 3.85 + nil(1:end-1);


%% DAE/ODE solver options
if p.octave
    1;
else
    % options to play with:
    opts = odeset('RelTol', 1e-6, ...
                     'AbsTol', 1e-8, ...
                     'MaxOrder', 5, ...  % order of stepper 1-5.  Using 2 or 1 is more stable but less accurate
                     'NormControl', 'off', ...  % if 'on' use less strigient error control
                     'BDF', 'off');      % use backwards differention formulas
    p.odeopts = opts;
end



%% Time
p.tspan = [0:565]*p.day;
