function animate(p, vars, pau, inds, varnames)
% animate(para, vars, pause, inds, varnames)
%
% Animates variables in time.  Examples:
%
% animate(para, phi)
% animate(para, {phi, channelized}, 0.1, 1:20)

% This m-file part of https://bitbucket.org/maurow/1dhydro
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

% defaults
if ~exist('pau', 'var') || isempty(pau)
    pau = 0;
end
if ~exist('inds', 'var') || isempty(inds)
    inds = 1:length(p.tspan);
end
if ~exist('varnames', 'var') || isempty(varnames)
    varnames = {};
end

x = {p.coords, (p.coords(1:end-1)+p.coords(2:end))/2};

axs = [];
if ~iscell(vars)
    vars = {vars};
end

% setup subplots
nsub = length(vars);
ylims = {};
xs = [];
for i=1:nsub
    axs(end+1) = subplot(nsub, 1, i);
    ylims{end+1} = [min(min(vars{i}(:,inds))), max(max(vars{i}(:,inds)))];
    if diff(ylims{end})==0
        ylims{end} = ylims{end} + [0,1];
    end
    if length(vars{i}(:,1))==length(x{1})
        xs(end+1) = 1;
    else
        xs(end+1) = 2;
    end
end
start = 1;
% do animation
for i=inds
    for j=1:nsub
        axes(axs(j));
        plot(x{xs(j)}, vars{j}(:,i))
        if j==1
            hold on
            plot(x{1}, p.phi_0/1e6)
            hold off
        end
        if ~isempty(varnames)
            ylabel(varnames{j})
        end
        if j==1
            title(['time = ', num2str(p.tspan(i)/p.day), ' (days)'])
            if start==1
                pause
                start = 2;
            end
        end
        ylim(ylims{j})
    end
    xlabel('x (m)')
    drawnow();
    pause(pau)
end
